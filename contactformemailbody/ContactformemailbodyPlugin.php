<?php
/**
 * Contact Form Emailbody plugin for Craft CMS
 *
 * Adds sender name and email address to the Contact Form email body.
 *
 * @author    Marijn Scholtus
 * @copyright Copyright (c) 2017 Thinkbright
 * @link      http://thinkbright.nl
 * @package   ContactformEmailbody
 * @since     1.0.0
 */

namespace Craft;

class ContactformemailbodyPlugin extends BasePlugin
{
    /**
     * @return mixed
     */
    public function init()
    {
        parent::init();

        craft()->on('contactForm.beforeMessageCompile', function(ContactFormMessageEvent $event) {
        $postedMessage = $event->params['postedMessage'];

        $savedBody = false;

        if (is_array($postedMessage))
        {
          // Capture all of the message fields on the model in case there's a validation error
          $event->messageFields = $postedMessage;

          // Add name and email to message body
          $postedMessage['Naam'] = craft()->request->getPost('fromName');
          $postedMessage['Email'] = craft()->request->getPost('fromEmail');

          // Capture the original message body
          if (isset($postedMessage['body']))
          {
            // Save the message body in case we need to reassign it in the event there's a validation error
            $savedBody = $postedMessage['body'];
          }

          // If it's false, then there was no messages[body] input submitted.  If it's '', then validation needs to fail.
          if ($savedBody === false || $savedBody !== '')
          {
            // Compile the message from each of the individual values
            $compiledMessage = '';

            foreach ($postedMessage as $key => $value)
            {
              if ($key != 'body')
              {
                if ($compiledMessage)
                {
                  $compiledMessage .= "  \n";
                }

                $compiledMessage .= $key.': ';

                if (is_array($value))
                {
                  $compiledMessage .= implode(', ', $value);
                }
                else
                {
                  $compiledMessage .= $value;
                }
              }
            }

            if (!empty($postedMessage['body']))
            {
              if ($compiledMessage)
              {
                $compiledMessage .= "\n\n";
              }

              $compiledMessage .= $postedMessage['body'];
            }

            $event->message = $compiledMessage;
          }
        }
        else
        {
          $event->message = $postedMessage;
          $event->messageFields = array('body' => $postedMessage);
        }
        });
    }

    /**
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('Contact Form Emailbody');
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('Adds sender name and email address to the Contact Form email body.');
    }

    /**
     * @return string
     */
    public function getDocumentationUrl()
    {
        return '???';
    }

    /**
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getDeveloper()
    {
        return 'Thinkbright';
    }

    /**
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://thinkbright.nl';
    }

    /**
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     */
    public function onBeforeInstall()
    {
    }

    /**
     */
    public function onAfterInstall()
    {
    }

    /**
     */
    public function onBeforeUninstall()
    {
    }

    /**
     */
    public function onAfterUninstall()
    {
    }
}
