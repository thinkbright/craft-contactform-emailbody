# Thinkbright Contactform Emailbody Changelog

## 1.0.0 -- 2017.04.12

* Initial release

Brought to you by [Marijn Scholtus](http://thinkbright.nl)
