# Thinkbright Contactform Emailbody plugin for Craft CMS

Adds sender name and email address to the email body.

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Thinkbright Contactform Emailbody, follow these steps:

1. Download & unzip the file and place the `thinkbrightcontactformemailbody` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /thinkbrightcontactformemailbody`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `thinkbrightcontactformemailbody` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Thinkbright Contactform Emailbody works on Craft 2.4.x and Craft 2.5.x.

## Thinkbright Contactform Emailbody Overview

-Insert text here-

## Configuring Thinkbright Contactform Emailbody

-Insert text here-

## Using Thinkbright Contactform Emailbody

-Insert text here-

## Thinkbright Contactform Emailbody Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Marijn Scholtus](http://thinkbright.nl)
